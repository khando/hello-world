const chai = require('chai');
const fibonnaci = require('../lib/fibonacci');

chai.should();

describe('fibonnaci', function () {
  context('n = 1', function () {
    it('should return 1', function () {
      fibonnaci(1).should.equal(1);
    });
  });

  context('n = 2', function () {
    it('should return 1', function () {
      fibonnaci(2).should.equal(1);
    });
  });

  context('n > 2', function () {
    it('should return right number', function () {
      fibonnaci(3).should.equal(2);
      fibonnaci(4).should.equal(3);
      fibonnaci(7).should.equal(13);
      fibonnaci(12).should.equal(144);
      fibonnaci(14).should.equal(377);
    });
  });
});
