const express = require('express');
const fibonacci = require('./lib/fibonacci');

const app = express();
const port = process.env.PORT || 5000;

app.get('/', (_req, res) => {
  res.json({
    message: 'Hello world',
  });
});

app.get('/fibonacci/:n', (req, res) => {
  const n = Number(req.params.n);
  const result = fibonacci(n);
  res.json({
    n,
    result,
  });
});

app.listen(port);
console.log(`Server is listening on port ${port}`);
