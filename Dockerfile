FROM node:10.13

WORKDIR /usr/src/app

COPY package.json yarn.lock ./

RUN yarn

COPY . .

USER node

CMD [ "yarn", "start" ]
